package com.wild.touche.paytouch.celebrities.data

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.domain.FilterParams
import com.wild.touche.paytouch.celebrities.logd
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


interface FilterParamsRepo {
    fun observe(): Observable<FilterParams>
    fun setFilter(params: FilterParams): Completable
}


class InMemoryFilterParamsRepo(private val injector: AppComponent) : FilterParamsRepo {

    private val scheduler by lazy { injector.workScheduler() }

    private val subject: BehaviorSubject<FilterParams> = BehaviorSubject.createDefault(FilterParams())

    override fun observe(): Observable<FilterParams> =
            subject.subscribeOn(scheduler)

    override fun setFilter(params: FilterParams): Completable = Completable.fromAction {
        logd { "set filter params: $params" }
        subject.onNext(params)
    }.subscribeOn(scheduler)

}