package com.wild.touche.paytouch.celebrities.di.android

import android.content.Context
import com.wild.touche.paytouch.celebrities.actorlist.ActorListUseCase
import com.wild.touche.paytouch.celebrities.actorlist.ActorListUseCaseImpl
import com.wild.touche.paytouch.celebrities.core.actions.ActionHandler
import com.wild.touche.paytouch.celebrities.core.actions.ViewModelActionHandler
import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import com.wild.touche.paytouch.celebrities.filter.FilterUseCase
import com.wild.touche.paytouch.celebrities.filter.FilterUseCaseImpl
import com.wild.touche.paytouch.celebrities.logd
import com.wild.touche.paytouch.celebrities.mainscreen.MainScreenUseCase
import com.wild.touche.paytouch.celebrities.mainscreen.MainScreenUseCaseImpl


class AndroidViewModelComponent(
        private val vm: FragmentViewModel
) : ViewModelComponent,
        AppComponent by vm.appComponent()
{

    private val actionHandler by lazy { ViewModelActionHandler(this) }

    override fun context(): Context {
        logd { "request context" }
        return vm.context()
    }
    override fun actionHandler(): ActionHandler = actionHandler

    override fun mainScreenUseCase(): MainScreenUseCase = MainScreenUseCaseImpl(this)
    override fun actorListUseCase(): ActorListUseCase = ActorListUseCaseImpl(this)
    override fun filterUseCase(): FilterUseCase = FilterUseCaseImpl(this)

}