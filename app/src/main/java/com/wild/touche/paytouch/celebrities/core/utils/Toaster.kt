package com.wild.touche.paytouch.celebrities.core.utils


interface Toaster {
    fun show(message: String)
}