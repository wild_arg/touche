package com.wild.touche.paytouch.celebrities.mainscreen

import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import com.wild.touche.paytouch.celebrities.domain.SortParams
import io.reactivex.Completable


interface MainScreenUseCase {
    fun orderByName(): Completable
    fun orderByPopularity(): Completable
}


class MainScreenUseCaseImpl(injector: ViewModelComponent) : MainScreenUseCase {

    val sortParamsRepo by lazy { injector.sortParamsRepo() }

    override fun orderByName(): Completable = sortParamsRepo.setSort(SortParams.SORT_BY_NAME)

    override fun orderByPopularity(): Completable = sortParamsRepo.setSort(SortParams.SORT_BY_POPULARITY)

}