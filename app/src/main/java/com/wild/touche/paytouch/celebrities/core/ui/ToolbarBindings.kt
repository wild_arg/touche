package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.support.annotation.MenuRes
import android.support.v7.widget.Toolbar


@BindingAdapter("support")
fun toolbarSupportBinding(toolbar: Toolbar, support: Boolean) {
    if (support)
        toolbar.asBaseActivity()?.setSupportActionBar(toolbar)
}

@BindingAdapter("backDisabled")
fun toolbarBackDisabledBinding(toolbar: Toolbar, backDisabled: Boolean) {
    if (backDisabled) {
        toolbar.navigationIcon = null
        toolbar.setNavigationOnClickListener(null)
    }
}

@BindingAdapter("menu", "clickListener", requireAll = false)
fun toolbarMenuBinding(toolbar: Toolbar, @MenuRes menu: Int, clickListener: android.widget.Toolbar.OnMenuItemClickListener?) {
    if (menu != 0) toolbar.inflateMenu(menu)
}