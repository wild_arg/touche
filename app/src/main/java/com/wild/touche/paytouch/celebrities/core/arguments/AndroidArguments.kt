package com.wild.touche.paytouch.celebrities.core.arguments

import android.os.Bundle


class AndroidArguments(private val bundle: Bundle) : Arguments {

    fun getBundle() = bundle

}