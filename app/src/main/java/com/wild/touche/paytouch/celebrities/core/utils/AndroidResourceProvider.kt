package com.wild.touche.paytouch.celebrities.core.utils

import com.wild.touche.paytouch.celebrities.di.AppComponent


class AndroidResourceProvider(private val injector: AppComponent) : ResourceProvider {

    override fun getString(resId: Int, vararg args: Any): String =
            injector.appContext().getString(resId, *args)

}