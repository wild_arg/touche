package com.wild.touche.paytouch.celebrities.filter

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.view.View
import com.wild.touche.paytouch.celebrities.R
import com.wild.touche.paytouch.celebrities.core.ui.asBaseActivity
import com.wild.touche.paytouch.celebrities.core.ui.onChange
import com.wild.touche.paytouch.celebrities.core.ui.setTo
import com.wild.touche.paytouch.celebrities.databinding.ScreenFilterBinding
import com.wild.touche.paytouch.celebrities.di.android.BaseFragment
import com.wild.touche.paytouch.celebrities.di.android.FragmentViewModel
import com.wild.touche.paytouch.celebrities.domain.FilterParams
import com.wild.touche.paytouch.celebrities.mainscreen.MainActivity


class FilterFragment : BaseFragment<FilterViewModel, ScreenFilterBinding>() {

    override val vmClass = FilterViewModel::class.java
    override val layoutId = R.layout.screen_filter

    override fun onBind(binding: ScreenFilterBinding, vm: FilterViewModel) {
        binding.vm = vm
    }

}


class FilterViewModel : FragmentViewModel() {

    private val useCase by inject { filterUseCase() }
    private val rp by inject { resourceProvider() }
    private val toaster by inject { toaster() }

    val name = ObservableField<String>()
    val popularity = ObservableField<Int>()
    val popularityLabel = ObservableField<String>()
    val location = ObservableField<String>()
    val locations = ObservableField<List<String>>(emptyList())
    val isTop = ObservableBoolean()
    val isNotTop = ObservableBoolean()

    init {
        popularity.onChange { refreshLabel(get() ?: 0) }
    }

    override fun onFragmentCreate() {
        super.onFragmentCreate()
        holder.resetCompositeDisposable()

        useCase.observeParams()
                .setTo(popularity) { it.minPopularity }
                .setTo(popularityLabel) { rp.getString(R.string.popularity_label, it.minPopularity) }
                .bindSubscribe()

        useCase.observeLocations()
                .setTo(locations)
                .bindSubscribe()
    }

    private fun refreshLabel(popularity: Int) {
        popularityLabel.set(rp.getString(R.string.popularity_label, popularity))
    }

    private fun prepareParams() = FilterParams(
            name = name.get(),
            location = location.get(),
            isTop = isTop.get(),
            minPopularity = popularity.get() ?: 0
    )

    fun clear() {
        name.set("")
        location.set("")
        isTop.set(false)
        isNotTop.set(false)
        popularity.set(0)
        toaster.show("Filter was cleared")
    }

    fun search(view: View) {
        (view.asBaseActivity() as? MainActivity)?.closeDrawer()
        useCase.setParams(prepareParams())
                .bindSubscribe()
    }

}