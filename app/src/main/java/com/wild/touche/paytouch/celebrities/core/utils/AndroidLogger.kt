package com.wild.touche.paytouch.celebrities.core.utils

import android.util.Log


private const val LOG_TAG = "Celebrities:"
class AndroidLogger : Logger {
    override fun d(msg: String) {
        Log.d(LOG_TAG, msg)
    }

    override fun e(msg: String) {
        Log.e(LOG_TAG, msg)
    }
}