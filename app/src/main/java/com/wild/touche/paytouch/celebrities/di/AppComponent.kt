package com.wild.touche.paytouch.celebrities.di

import android.content.Context
import com.wild.touche.paytouch.celebrities.core.rx.CompositeDisposableHolder
import com.wild.touche.paytouch.celebrities.core.utils.Formatter
import com.wild.touche.paytouch.celebrities.core.utils.Logger
import com.wild.touche.paytouch.celebrities.core.utils.ResourceProvider
import com.wild.touche.paytouch.celebrities.core.utils.Toaster
import com.wild.touche.paytouch.celebrities.data.ActorsRepo
import com.wild.touche.paytouch.celebrities.data.FilterParamsRepo
import com.wild.touche.paytouch.celebrities.data.LocationRepo
import com.wild.touche.paytouch.celebrities.data.SortParamsRepo
import com.wild.touche.paytouch.celebrities.mainscreen.network.GetActorsPageRequest
import com.wild.touche.paytouch.celebrities.mainscreen.network.GetActorsRequest
import com.wild.touche.paytouch.celebrities.network.api.CelebritiesApi
import io.reactivex.Scheduler


interface AppComponent {
    fun appContext(): Context
    fun compositeDisposableHolder(): CompositeDisposableHolder

    fun logger(): Logger
    fun toaster(): Toaster
    fun formatter(): Formatter
    fun resourceProvider(): ResourceProvider

    fun workScheduler(): Scheduler
    fun networkScheduler(): Scheduler
    fun uiScheduler(): Scheduler

    fun actorsRepo(): ActorsRepo
    fun sortParamsRepo(): SortParamsRepo
    fun filterParamsRepo(): FilterParamsRepo
    fun locationsRepo(): LocationRepo

    fun apiClient(): CelebritiesApi
    fun getActorsRequest(): GetActorsRequest
    fun getActorsPageRequest(page: Int): GetActorsPageRequest
}