package com.wild.touche.paytouch.celebrities.actorlist

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.view.View
import com.wild.touche.paytouch.celebrities.R
import com.wild.touche.paytouch.celebrities.core.ui.setTo
import com.wild.touche.paytouch.celebrities.databinding.ScreenActorListBinding
import com.wild.touche.paytouch.celebrities.di.android.BaseFragment
import com.wild.touche.paytouch.celebrities.di.android.FragmentViewModel


class ActorListFragment : BaseFragment<ActorListViewModel, ScreenActorListBinding>() {

    override val vmClass = ActorListViewModel::class.java
    override val layoutId = R.layout.screen_actor_list

    override fun onBind(binding: ScreenActorListBinding, vm: ActorListViewModel) {
        binding.vm = vm
    }

}


class ActorListViewModel : FragmentViewModel() {

    private val useCase by inject { actorListUseCase() }
    private val formatter by inject { formatter() }
    private val toaster by inject { toaster() }

    val showLoader = ObservableBoolean()
    val actors = ObservableField<List<ActorModelWrapper>>(emptyList())

    override fun onFragmentCreate() {
        if (actors.get()!!.isEmpty())
            initObserver()
    }

    fun onRefresh() {
        useCase.refresh()
                .doOnSubscribe { showLoader.set(true) }
                .doFinally { showLoader.set(false) }
                .bindSubscribe(
                        onError = { toaster.show("Error on request") }
                )
    }

    fun loadMore(view: View) {
        useCase.loadNextPage()
                .doOnSubscribe { showLoader.set(true) }
                .doFinally { showLoader.set(false) }
                .bindSubscribe(
                        onError = { toaster.show("Error on request") }
                )
    }

    private fun initObserver() {
        useCase.observeActors()
                .setTo(actors) { it.map { ActorModelWrapper(it, formatter) } }
                .doOnSubscribe { holder.resetCompositeDisposable() }
                .doOnSubscribe { onRefresh() }
                .bindSubscribe(
                        onError = { toaster.show("Error on request") }
                )
    }

}
