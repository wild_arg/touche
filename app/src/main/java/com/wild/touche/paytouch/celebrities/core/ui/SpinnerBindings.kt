package com.wild.touche.paytouch.celebrities.core.ui

import android.content.Context
import android.databinding.BindingAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Spinner
import com.wild.touche.paytouch.celebrities.R


@BindingAdapter("dropdownVOffset")
fun dropdownVOffsetBinding(spinner: Spinner, offset: Float) {
    spinner.dropDownVerticalOffset = offset.toInt()
    spinner.adapter = CustomMenuAdapter(spinner.context)
}

class CustomMenuAdapter(val context: Context) : BaseAdapter() {

    val items = listOf("Sort by Ascending", "Sort by Descending")

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View =
            convertView ?:
                    when (position) {
                        0   -> View.inflate(context, R.layout.menu_selected_layout, parent)
                        else    -> View.inflate(context, R.layout.menu_dropdown_layout, parent)
                    }

    override fun getItem(position: Int): String =
            if (position == 0) "" else items[position - 1]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.size + 1

    override fun getItemViewType(position: Int): Int =
            if (position == 0) 0 else 1

}