package com.wild.touche.paytouch.celebrities.data

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.domain.ActorModel
import com.wild.touche.paytouch.celebrities.logd
import com.wild.touche.paytouch.celebrities.loge
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


interface ActorsRepo {
    fun observe(): Observable<List<ActorModel>>
    fun pull(page: Int): Completable
    fun clear(): Completable
}

class InMemoryActorsRepo(private val injector: AppComponent) : ActorsRepo {

    private val scheduler by lazy { injector.workScheduler() }

    private val map: HashMap<Int, List<ActorModel>> = HashMap()
    private val subject: BehaviorSubject<HashMap<Int, List<ActorModel>>> = BehaviorSubject.createDefault(map)

    override fun observe(): Observable<List<ActorModel>> =
            subject.map { it.toList().map { it.second } }
                    .map { it.flatten() }
                    .subscribeOn(scheduler)

    override fun pull(page: Int): Completable =
        injector.getActorsPageRequest(page).asSingle()
                .doOnSuccess {
                    map[page] = it
                    subject.onNext(map)
                }
                .doOnSubscribe { logd { "pull $page page..." } }
                .doOnError { loge { "error on pulling: $it" } }
                .subscribeOn(scheduler)
                .toCompletable()

    override fun clear(): Completable = Completable.fromAction {
        map.clear()
        subject.onNext(map)
    }.subscribeOn(scheduler)
}