package com.wild.touche.paytouch.celebrities.mainscreen.network

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.domain.ActorModel
import com.wild.touche.paytouch.celebrities.network.Request
import io.reactivex.Single


class GetActorsPageRequest(injector: AppComponent, val page: Int) : Request<List<ActorModel>>(injector) {

    override fun execute(): Single<List<ActorModel>> =
            api.actors(page).map { it.data }

}