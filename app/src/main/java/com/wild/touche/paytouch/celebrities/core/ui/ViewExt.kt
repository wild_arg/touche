package com.wild.touche.paytouch.celebrities.core.ui

import android.content.Context
import android.content.ContextWrapper
import android.view.View
import com.wild.touche.paytouch.celebrities.di.android.BaseActivity


var View.visible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {visibility = if (value) View.VISIBLE else View.GONE}

fun View.asBaseActivity() = context.asActivity<BaseActivity>()

inline fun <reified T : Any> Context.asActivity(): T? {
    var context: Context? = this
    while (context !is T && context != null) {
        context = if (context is ContextWrapper) context.baseContext else null
    }
    return context as T?
}