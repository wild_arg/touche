package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.text.Html
import android.widget.TextView


@BindingAdapter("fromHtml")
fun textViewFromHtmlBinding(textView: TextView, fromHtml: String) {
    textView.text = Html.fromHtml(fromHtml)
}