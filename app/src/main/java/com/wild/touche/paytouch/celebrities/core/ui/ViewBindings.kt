package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.view.View


@BindingAdapter("visible")
fun viewVisibleBinding(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}
