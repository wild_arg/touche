package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView


@BindingAdapter("items")
fun autocompleteItemsBinding(autocomplete: AutoCompleteTextView, items: List<String>) {
    autocomplete.setAdapter(ArrayAdapter(autocomplete.context, android.R.layout.simple_dropdown_item_1line, items))
}

@BindingAdapter("dropDownOnClick")
fun autocompleteDropDownOnClickBindings(autocomplete: AutoCompleteTextView, dropDownOnClick: Boolean) {
    if (dropDownOnClick)
        autocomplete.setOnClickListener { autocomplete.showDropDown() }
}