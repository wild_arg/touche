package com.wild.touche.paytouch.celebrities.core.utils


interface Logger {
    fun d(msg: String)
    fun e(msg: String)
}

class DefaultSystemLogger : Logger {
    override fun d(msg: String) = println(msg)
    override fun e(msg: String) = println("error: $msg")
}