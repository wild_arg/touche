package com.wild.touche.paytouch.celebrities.core.utils


interface ResourceProvider {
    fun getString(resId: Int, vararg args: Any): String
}