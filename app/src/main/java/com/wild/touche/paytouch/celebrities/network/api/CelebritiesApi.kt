package com.wild.touche.paytouch.celebrities.network.api

import com.wild.touche.paytouch.celebrities.domain.ActorModel
import com.wild.touche.paytouch.celebrities.network.DataResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface CelebritiesApi {
    @GET("/rest/actors")
    fun actors(@Query("page") page: Int = 1): Single<DataResponse<List<ActorModel>>>
}