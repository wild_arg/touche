package com.wild.touche.paytouch.celebrities.di.android

import android.content.Context
import com.google.gson.Gson
import com.wild.touche.paytouch.celebrities.BuildConfig
import com.wild.touche.paytouch.celebrities.core.rx.CommonCompositeDisposableHolder
import com.wild.touche.paytouch.celebrities.core.utils.AndroidFormatter
import com.wild.touche.paytouch.celebrities.core.utils.AndroidLogger
import com.wild.touche.paytouch.celebrities.core.utils.AndroidResourceProvider
import com.wild.touche.paytouch.celebrities.core.utils.AndroidToaster
import com.wild.touche.paytouch.celebrities.core.utils.Formatter
import com.wild.touche.paytouch.celebrities.core.utils.Logger
import com.wild.touche.paytouch.celebrities.core.utils.ResourceProvider
import com.wild.touche.paytouch.celebrities.core.utils.Toaster
import com.wild.touche.paytouch.celebrities.data.ActorsRepo
import com.wild.touche.paytouch.celebrities.data.FilterParamsRepo
import com.wild.touche.paytouch.celebrities.data.InMemoryActorsRepo
import com.wild.touche.paytouch.celebrities.data.InMemoryFilterParamsRepo
import com.wild.touche.paytouch.celebrities.data.InMemoryLocationRepo
import com.wild.touche.paytouch.celebrities.data.InMemorySortParamsRepo
import com.wild.touche.paytouch.celebrities.data.LocationRepo
import com.wild.touche.paytouch.celebrities.data.SortParamsRepo
import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.mainscreen.network.GetActorsPageRequest
import com.wild.touche.paytouch.celebrities.mainscreen.network.GetActorsRequest
import com.wild.touche.paytouch.celebrities.network.api.CelebritiesApi
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class AndroidAppComponent(private val app: BaseApp) : AppComponent {

    private val logger: Logger by lazy { AndroidLogger() }
    private val toaster: Toaster by lazy { AndroidToaster(this) }
    private val resourceProvider: ResourceProvider by lazy { AndroidResourceProvider(this) }
    private val formatter: Formatter by lazy { AndroidFormatter() }
    private val actorsRepo: ActorsRepo by lazy { InMemoryActorsRepo(this) }
    private val sortParamsRepo: SortParamsRepo by lazy { InMemorySortParamsRepo(this) }
    private val filterParamsRepo: FilterParamsRepo by lazy { InMemoryFilterParamsRepo(this) }
    private val locationsRepo: LocationRepo by lazy { InMemoryLocationRepo(this) }

    override fun appContext(): Context = app
    override fun compositeDisposableHolder() = CommonCompositeDisposableHolder()

    override fun logger(): Logger = logger
    override fun toaster(): Toaster = toaster
    override fun formatter(): Formatter = formatter
    override fun resourceProvider(): ResourceProvider = resourceProvider

    override fun workScheduler(): Scheduler = Schedulers.io()
    override fun networkScheduler(): Scheduler = Schedulers.single()
    override fun uiScheduler(): Scheduler = AndroidSchedulers.mainThread()

    override fun actorsRepo(): ActorsRepo = actorsRepo
    override fun sortParamsRepo(): SortParamsRepo = sortParamsRepo
    override fun filterParamsRepo(): FilterParamsRepo = filterParamsRepo
    override fun locationsRepo(): LocationRepo = locationsRepo

    override fun apiClient(): CelebritiesApi = retrofit().create(CelebritiesApi::class.java)
    override fun getActorsRequest(): GetActorsRequest = GetActorsRequest(this)
    override fun getActorsPageRequest(page: Int): GetActorsPageRequest = GetActorsPageRequest(this, page)

    private fun retrofit() = Retrofit.Builder()
            .client(okHttp())
            .baseUrl(BuildConfig.END_POINT)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()

    private fun okHttp() = OkHttpClient.Builder()
            .build()
}

