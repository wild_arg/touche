package com.wild.touche.paytouch.celebrities.core.utils

import java.text.NumberFormat
import java.util.Locale


class AndroidFormatter : Formatter {

    private val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault()).apply {
        maximumFractionDigits = 2
    }

    override fun formatDouble(d: Double) = numberFormat.format(d)
}