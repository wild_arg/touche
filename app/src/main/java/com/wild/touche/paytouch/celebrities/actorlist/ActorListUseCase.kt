package com.wild.touche.paytouch.celebrities.actorlist

import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import com.wild.touche.paytouch.celebrities.domain.ActorModel
import com.wild.touche.paytouch.celebrities.domain.FilterParams
import com.wild.touche.paytouch.celebrities.domain.SortParams
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.functions.Function3


interface ActorListUseCase {
    fun observeActors(): Observable<List<ActorModel>>
    fun refresh(): Completable
    fun loadNextPage(): Completable
}


class ActorListUseCaseImpl(injector: ViewModelComponent) : ActorListUseCase {

    private val actorsRepo by lazy { injector.actorsRepo() }
    private val sortParamsRepo by lazy { injector.sortParamsRepo() }
    private val locationRepo by lazy { injector.locationsRepo() }
    private val filterRepo by lazy { injector.filterParamsRepo() }
    private var page: Int = 1

    override fun observeActors(): Observable<List<ActorModel>> =
            Observable.combineLatest(
                    actorsRepo.observe().flatMap { updateLocations(it) },
                    sortParamsRepo.observe(),
                    filterRepo.observe(),
                    Function3<List<ActorModel>, SortParams, FilterParams, List<ActorModel>> { list, sort, filter ->
                        list.apply(sort).apply(filter)
                    }
            )
                    .doOnSubscribe { actorsRepo.pull(page) }

    override fun refresh(): Completable =
            actorsRepo.clear()
                    .andThen(locationRepo.clear())
                    .andThen(actorsRepo.pull(1))
                    .doOnComplete { page = 1 }

    override fun loadNextPage(): Completable =
            actorsRepo.pull(page + 1)
                    .doOnComplete { page++ }

    private fun updateLocations(actors: List<ActorModel>): Observable<List<ActorModel>> =
            locationRepo.add(actors.map { it.location })
                    .toSingleDefault(actors)
                    .toObservable()

    private fun List<ActorModel>.apply(sort: SortParams) =
            when (sort) {
                SortParams.SORT_BY_NAME -> sortedBy { it.name }
                SortParams.SORT_BY_POPULARITY   -> sortedBy { it.popularity }
                else    -> this
            }

    private fun List<ActorModel>.apply(filter: FilterParams) =
            filter { actor ->
                filter.name?.let { actor.name.contains(it, true) } ?: true
                && filter.location?.let { actor.location.contains(it, true) } ?: true
                && filter.isTop?.let { actor.top == it } ?: true
                && filter.minPopularity.let { actor.popularity >= it }
            }

}