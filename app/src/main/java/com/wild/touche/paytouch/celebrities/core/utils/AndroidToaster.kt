package com.wild.touche.paytouch.celebrities.core.utils

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.wild.touche.paytouch.celebrities.di.AppComponent


class AndroidToaster(private val injector: AppComponent) : Toaster {

    override fun show(message: String) {
        Handler(Looper.getMainLooper()).post { Toast.makeText(injector.appContext(), message, Toast.LENGTH_SHORT).show() }
    }

}