package com.wild.touche.paytouch.celebrities.core.rx

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


interface CompositeDisposableHolder {
    var composite: CompositeDisposable

    fun Disposable.bind()

    fun resetCompositeDisposable() {
        synchronized(this) {
            composite.clear()
            composite = CompositeDisposable()
        }
    }
}

class CommonCompositeDisposableHolder : CompositeDisposableHolder {
    override var composite = CompositeDisposable()

    override fun Disposable.bind() {
        composite.add(this)
    }
}

private val onNextStub: (Any) -> Unit = {}
private val onErrorStub: (Throwable) -> Unit = {}
private val onCompleteStub: () -> Unit = {}

interface BindSubscriber {

    val holder: CompositeDisposableHolder

    fun <T : Any> Observable<T>.bindSubscribe(
            onNext: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub,
            onComplete: () -> Unit = onCompleteStub
    ) = holder.run { subscribe(onNext, onError, onComplete).bind() }

    fun <T : Any> Single<T>.bindSubscribe(
            onNext: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub
    ) = holder.run { subscribe(onNext, onError).bind() }

    fun Completable.bindSubscribe(
            onError: (Throwable) -> Unit = onErrorStub,
            onComplete: () -> Unit = onCompleteStub
    ) = holder.run { subscribe(onComplete, onError).bind() }

}