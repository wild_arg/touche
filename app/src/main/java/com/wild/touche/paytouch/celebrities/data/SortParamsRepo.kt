package com.wild.touche.paytouch.celebrities.data

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.domain.SortParams
import com.wild.touche.paytouch.celebrities.logd
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


interface SortParamsRepo {
    fun observe(): Observable<SortParams>
    fun setSort(params: SortParams): Completable
}


class InMemorySortParamsRepo(private val injector: AppComponent) : SortParamsRepo {

    private val scheduler by lazy { injector.workScheduler() }

    private val subject: BehaviorSubject<SortParams> = BehaviorSubject.createDefault(SortParams.UNSORTED)

    override fun observe(): Observable<SortParams> =
            subject.subscribeOn(scheduler)

    override fun setSort(params: SortParams): Completable = Completable.fromAction {
        logd { "set sort param: ${params.name}" }
        subject.onNext(params)
    }.subscribeOn(scheduler)

}