package com.wild.touche.paytouch.celebrities

import com.wild.touche.paytouch.celebrities.core.utils.Logger
import com.wild.touche.paytouch.celebrities.di.android.BaseApp


class CelebritiesApp : BaseApp() {

    override fun onCreate() {
        super.onCreate()
        logger = appComponent.logger()
    }

    companion object {
        lateinit var logger: Logger
    }
}

inline fun logd(block: () -> String) = CelebritiesApp.logger.d(block())
inline fun loge(block: () -> String) = CelebritiesApp.logger.e(block())