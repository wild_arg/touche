package com.wild.touche.paytouch.celebrities.data

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.logd
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


interface LocationRepo {
    fun observe(): Observable<List<String>>
    fun add(locations: List<String>): Completable
    fun clear(): Completable
}


class InMemoryLocationRepo(injector: AppComponent) : LocationRepo {

    private val scheduler by lazy { injector.workScheduler() }

    private val subject: BehaviorSubject<Set<String>> = BehaviorSubject.createDefault(emptySet())

    override fun observe(): Observable<List<String>> =
            subject.map { it.toList() }
                    .subscribeOn(scheduler)

    override fun add(locations: List<String>): Completable = Completable.fromAction {
        logd { "add locations: $locations" }
        subject.onNext(subject.value.plus(locations))
    }

    override fun clear(): Completable = Completable.fromAction {
        subject.onNext(emptySet())
    }.subscribeOn(scheduler)
}