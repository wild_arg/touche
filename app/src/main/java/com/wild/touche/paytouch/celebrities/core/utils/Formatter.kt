package com.wild.touche.paytouch.celebrities.core.utils


interface Formatter {
    fun formatDouble(d: Double): String
}