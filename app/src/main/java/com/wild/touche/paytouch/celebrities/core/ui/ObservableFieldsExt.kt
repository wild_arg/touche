package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.Single


fun <T : Any> Observable<T>.setTo(field: ObservableField<T>) = doOnNext { field.set(it) }
fun <T : Any, F: Any> Observable<T>.setTo(field: ObservableField<F>, mapper: (T) -> F) = doOnNext { field.set(mapper(it)) }

fun <T : Any> Single<T>.setTo(field: ObservableField<T>) = doOnSuccess { field.set(it) }
fun <T : Any, F: Any> Single<T>.setTo(field: ObservableField<F>, mapper: (T) -> F) = doOnSuccess { field.set(mapper(it)) }

fun <T : Any?> ObservableField<T>.onChange(block: ObservableField<T>.() -> Unit) =
        addOnPropertyChangedCallback(ObservableFieldChangeCallback(this, block))

private class ObservableFieldChangeCallback<T : Any?>(
        private val field: ObservableField<T>,
        private val block: ObservableField<T>.() -> Unit
) : android.databinding.Observable.OnPropertyChangedCallback()
{
    override fun onPropertyChanged(sender: android.databinding.Observable?, propertyId: Int) {
        field.block()
    }
}