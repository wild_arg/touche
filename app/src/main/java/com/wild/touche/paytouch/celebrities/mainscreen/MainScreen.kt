package com.wild.touche.paytouch.celebrities.mainscreen

import android.databinding.ObservableBoolean
import android.support.v4.view.GravityCompat
import android.view.View
import com.wild.touche.paytouch.celebrities.R
import com.wild.touche.paytouch.celebrities.core.ui.asBaseActivity
import com.wild.touche.paytouch.celebrities.databinding.ScreenMainBinding
import com.wild.touche.paytouch.celebrities.di.android.BaseActivity
import com.wild.touche.paytouch.celebrities.di.android.BaseFragment
import com.wild.touche.paytouch.celebrities.di.android.FragmentViewModel
import kotlinx.android.synthetic.main.screen_main.*


class MainActivity : BaseActivity() {
    override fun newFragment() = MainFragment()

    fun openDrawer() {
        drawer_layout.openDrawer(GravityCompat.END)
    }

    fun closeDrawer() {
        drawer_layout.closeDrawer(GravityCompat.END)
    }

}

class MainFragment : BaseFragment<MainScreenViewModel, ScreenMainBinding>() {
    override val vmClass = MainScreenViewModel::class.java
    override val layoutId = R.layout.screen_main

    override fun onBind(binding: ScreenMainBinding, vm: MainScreenViewModel) {
        binding.vm = vm
    }

}

class MainScreenViewModel : FragmentViewModel() {

    val menuShowed = ObservableBoolean(false)

    private val useCase by inject { mainScreenUseCase() }

    fun toggleMenu() {
        menuShowed.set(!menuShowed.get())
    }

    fun sortByName() {
        useCase.orderByName()
                .doOnSubscribe { toggleMenu() }
                .bindSubscribe()
    }

    fun sortByPopularity() {
        useCase.orderByPopularity()
                .doOnSubscribe { toggleMenu() }
                .bindSubscribe()
    }

    fun openFilter(view: View) {
        (view.asBaseActivity() as? MainActivity)?.openDrawer()
    }
}