package com.wild.touche.paytouch.celebrities.actorlist

import com.wild.touche.paytouch.celebrities.core.utils.Formatter
import com.wild.touche.paytouch.celebrities.domain.ActorModel


class ActorModelWrapper(model: ActorModel, formatter: Formatter) {
    val name = model.name.let { it.replace(Regex("(^\\S+)"), "<b>$1</b>") }
    val location = model.location
    val description = model.description
    val photoUrl = model.photoUrl
    val popularity = formatter.formatDouble(model.popularity)
}