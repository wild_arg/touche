package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.support.v4.widget.DrawerLayout
import android.view.View


interface LinkDrawerHandler {
    fun linkDrawer(drawer: DrawerLayout)
}

@BindingAdapter("linkDrawer")
fun linkDrawerBinding(view: DrawerLayout, handler: LinkDrawerHandler) {
    handler.linkDrawer(view)
}

@BindingAdapter("hideKeyboardWhenClose")
fun hideKeyboardBinding(view: DrawerLayout, hideKeyboard: Boolean) {
    if (hideKeyboard)
        view.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) { }
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) { }
            override fun onDrawerOpened(drawerView: View) { }
            override fun onDrawerClosed(drawerView: View) { view.asBaseActivity()?.hideKeyboard() }
        })
}