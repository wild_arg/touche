package com.wild.touche.paytouch.celebrities.actorlist

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wild.touche.paytouch.celebrities.R
import com.wild.touche.paytouch.celebrities.databinding.ItemActorBinding
import com.wild.touche.paytouch.celebrities.databinding.ItemLoadMoreButtonBinding


@BindingAdapter("actors", "loadMore", requireAll = true)
fun actorsBinding(recycler: RecyclerView, actors: List<ActorModelWrapper>, loadMore: View.OnClickListener) {
    val adapter = recycler.adapter as? ActorsRecyclerAdapter
    if (adapter == null)
        recycler.adapter = ActorsRecyclerAdapter(actors, recycler.context, loadMore)
    else {
        adapter.actors = actors
        adapter.notifyDataSetChanged()
    }
}

private const val ACTOR_ITEM = 0
private const val BUTTON_ITEM = 1

class ActorsRecyclerAdapter(
        var actors: List<ActorModelWrapper>,
        private val context: Context,
        private val listener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ACTOR_ITEM)
            DataBindingUtil.inflate<ItemActorBinding>(
                    LayoutInflater.from(context),
                    R.layout.item_actor, parent,
                    false
            ).let { ActorViewHolder(it) }
        else
            DataBindingUtil.inflate<ItemLoadMoreButtonBinding>(
                    LayoutInflater.from(context),
                    R.layout.item_load_more_button, parent,
                    false
            ).let { ButtonViewHolder(it) }
    }

    override fun getItemViewType(position: Int): Int =
        if (position == actors.size) BUTTON_ITEM else ACTOR_ITEM

    override fun getItemCount(): Int = actors.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder is ActorViewHolder)
            holder.publish(actors[position], position % 2 == 1)
        else (holder as? ButtonViewHolder)?.publish(listener)
    }

}

class ActorViewHolder(private val binding: ItemActorBinding) : RecyclerView.ViewHolder(binding.root) {
    fun publish(model: ActorModelWrapper, light: Boolean) {
        binding.item = model
        binding.light = light
    }
}

class ButtonViewHolder(private val binding: ItemLoadMoreButtonBinding) : RecyclerView.ViewHolder(binding.root) {

    var listener: View.OnClickListener? = null

    fun publish(clickListener: View.OnClickListener) {
        binding.item = this
        listener = clickListener
    }

    fun onClick(view: View) {
        listener?.onClick(view)
    }
}