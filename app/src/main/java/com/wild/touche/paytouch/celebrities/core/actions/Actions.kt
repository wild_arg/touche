package com.wild.touche.paytouch.celebrities.core.actions

import android.content.Context
import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import io.reactivex.Completable


interface Action<P> {
    fun invoke(context: Context, params: P): Completable
    fun with(params: P) = ActionData(this, params)
}

class ActionData<P>(val action: Action<P>, val params: P)

abstract class ActivityAction<P>(val injector: ViewModelComponent) : Action<P> {
    protected inline fun <T> inject(crossinline injectBlock: ViewModelComponent.() -> T): Lazy<T> =
            lazy { injector.injectBlock() }
}


interface ActionsHelper {

    val ah: ActionHandler

    fun <P : Any> executeAction(block: () -> ActionData<P>) =
            block().let {
                ah.handleAction(it.action, it.params)
            }
}