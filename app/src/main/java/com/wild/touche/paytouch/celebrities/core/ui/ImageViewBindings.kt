package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions


@BindingAdapter("imageUrl", "circleCrop", requireAll = false)
fun imageViewUrlBinding(imageView: ImageView, imageUrl: String, circleCrop: Boolean = false) {
    val loader = Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions.overrideOf(300))
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
    if (circleCrop)
            loader.apply(RequestOptions.circleCropTransform())
    loader.into(imageView)
}