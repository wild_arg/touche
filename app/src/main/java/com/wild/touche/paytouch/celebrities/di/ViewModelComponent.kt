package com.wild.touche.paytouch.celebrities.di

import android.content.Context
import com.wild.touche.paytouch.celebrities.actorlist.ActorListUseCase
import com.wild.touche.paytouch.celebrities.core.actions.ActionHandler
import com.wild.touche.paytouch.celebrities.filter.FilterUseCase
import com.wild.touche.paytouch.celebrities.mainscreen.MainScreenUseCase


interface ViewModelComponent : AppComponent {

    fun context(): Context
    fun actionHandler(): ActionHandler

    fun mainScreenUseCase(): MainScreenUseCase
    fun actorListUseCase(): ActorListUseCase
    fun filterUseCase(): FilterUseCase
}