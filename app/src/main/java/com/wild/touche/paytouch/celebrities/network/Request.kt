package com.wild.touche.paytouch.celebrities.network

import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.logd
import com.wild.touche.paytouch.celebrities.loge
import io.reactivex.Single


abstract class Request<T>(protected val injector: AppComponent) {

    protected val api by inject { apiClient() }
    private val scheduler by inject { networkScheduler() }

    protected abstract fun execute(): Single<T>

    protected inline fun <T> inject(crossinline injectBlock: AppComponent.() -> T): Lazy<T> =
            lazy { injector.injectBlock() }

    fun asSingle(): Single<T> =
            execute()
                    .doOnSubscribe { logd { "start request ${this::class.java.simpleName}" } }
                    .doOnError { loge { "error on request ${this::class.java.simpleName}, $it" } }
                    .doOnSuccess { logd { "request ${this::class.java.simpleName} successfully completed" } }
                    .subscribeOn(scheduler)
}

class DataResponse<T>(val data: T)