package com.wild.touche.paytouch.celebrities.domain

import com.google.gson.annotations.SerializedName


class ActorModel(
        val identifier: String,
        val name: String,
        val location: String,
        val description: String,
        @SerializedName("profile_path")
        val photoUrl: String,
        val popularity: Double,
        val top: Boolean
)