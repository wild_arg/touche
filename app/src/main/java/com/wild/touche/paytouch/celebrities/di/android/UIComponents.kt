package com.wild.touche.paytouch.celebrities.di.android

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.wild.touche.paytouch.celebrities.R
import com.wild.touche.paytouch.celebrities.core.arguments.AndroidArguments
import com.wild.touche.paytouch.celebrities.core.arguments.Arguments
import com.wild.touche.paytouch.celebrities.core.rx.BindSubscriber
import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import java.lang.ref.WeakReference
import kotlin.reflect.KClass


abstract class BaseApp : Application() {
    val appComponent: AppComponent by lazy { AndroidAppComponent(this) }
    protected inline fun <T> inject(crossinline injectBlock: AppComponent.() -> T): Lazy<T> =
            lazy { appComponent.injectBlock() }
}

abstract class BaseActivity : AppCompatActivity() {
    val appComponent: AppComponent by lazy { (application as? BaseApp)?.appComponent
            ?: throw IllegalStateException("AppComponent is null") }

    abstract fun newFragment(): Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_container)
        if (savedInstanceState == null)
            newFragment().apply {
                arguments = intent.extras
                supportFragmentManager.beginTransaction()
                        .add(R.id.container, this)
                        .commit()
            }
    }

    fun hideKeyboard() {
        val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.currentFocus!!.windowToken, 0)
    }

    companion object {
        fun makeArguments(): Arguments = AndroidArguments(Bundle())
        fun setArguments(intent: Intent, args: Arguments) =
                (args as? AndroidArguments)?.let { intent.putExtras(args.getBundle()) }
    }

}

fun <T : BaseActivity> makeIntent(context: Context, activityClass: KClass<T>, argsBlock: Arguments.() -> Unit) =
        Intent(context, activityClass.java).apply {
            BaseActivity.setArguments(this, BaseActivity.makeArguments().apply { argsBlock() })
        }


abstract class BaseFragment<VM : FragmentViewModel, in VB : ViewDataBinding> : Fragment() {

    val appComponent: AppComponent by lazy { (activity as? BaseActivity)?.appComponent
            ?: throw IllegalStateException("AppComponent is null") }

    protected abstract val vmClass: Class<VM>
    protected abstract val layoutId: Int

    lateinit var vm: VM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vm = ViewModelProviders.of(this)[vmClass]
        val binding = DataBindingUtil.inflate<VB>(inflater, layoutId, container, false)
        vm.arguments = AndroidArguments(arguments ?: Bundle())
        vm.fragment = WeakReference(this)
        vm.onFragmentCreate()
        onBind(binding, vm)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        vm.fragment?.clear()
    }

    protected abstract fun onBind(binding: VB, vm: VM)

}

abstract class FragmentViewModel : ViewModel(), BindSubscriber {

    protected val vmComponent: ViewModelComponent by lazy { AndroidViewModelComponent(this) }
    internal var fragment: WeakReference<BaseFragment<*, *>>? = null
    lateinit var arguments: Arguments

    override val holder by inject { compositeDisposableHolder() }

    fun appComponent(): AppComponent = fragment?.get()?.appComponent ?: throw IllegalStateException("AppComponent is null")
    fun context(): Context = fragment?.get()?.context ?: throw IllegalStateException("Fragment context is not available")

    protected inline fun <T> inject(crossinline injectBlock: ViewModelComponent.() -> T): Lazy<T> =
            lazy { vmComponent.injectBlock() ?: throw IllegalStateException("injector is null") }
    protected fun args(): Arguments = arguments

    open fun onFragmentCreate() { }

    override fun onCleared() {
        holder.resetCompositeDisposable()
        fragment?.clear()

        super.onCleared()
    }

}

