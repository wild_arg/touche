package com.wild.touche.paytouch.celebrities.domain


data class FilterParams(
        val name: String? = null,
        val location: String? = null,
        val isTop: Boolean? = null,
        val minPopularity: Int = 0
)