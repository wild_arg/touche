package com.wild.touche.paytouch.celebrities.domain


class SortParams(val name: String) {
    companion object {
        val UNSORTED = SortParams("Unsorted")
        val SORT_BY_NAME = SortParams("SortByName")
        val SORT_BY_POPULARITY = SortParams("SortByPopularity")
    }
}