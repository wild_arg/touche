package com.wild.touche.paytouch.celebrities.core.actions

import android.content.Context
import com.wild.touche.paytouch.celebrities.core.rx.CompositeDisposableHolder
import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.di.ViewModelComponent
import com.wild.touche.paytouch.celebrities.logd
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.disposables.Disposable


interface ActionHandler {
    fun <P> handleAction(action: Action<P>, params: P)
    fun stopActions(): Completable
}

abstract class BaseActionHandler(private val injector: AppComponent
) : ActionHandler,
        CompositeDisposableHolder by injector.compositeDisposableHolder()
{

    protected open fun context(): Context = injector.appContext()

    override fun <P> handleAction(action: Action<P>, params: P) {
        val observer = createObserver(action)
        action.invoke(context(), params)
                .subscribeOn(injector.workScheduler())
                .subscribeWith(observer)
        observer.disposable?.bind()
    }

    private fun <P> createObserver(action: Action<P>) = object : CompletableObserver {
        var disposable: Disposable? = null

        override fun onComplete() {
            disposable?.let(composite::remove)
            logd { "action ${action::class.java.simpleName} was executed" }
        }

        override fun onSubscribe(d: Disposable) {
            disposable = d
        }

        override fun onError(e: Throwable) {
            logd { "error $e" }
        }
    }

    override fun stopActions() = Completable.fromAction { resetCompositeDisposable() }
}

class ViewModelActionHandler(private val injector: ViewModelComponent) : BaseActionHandler(injector) {
    override fun context(): Context = injector.context()
}