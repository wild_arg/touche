package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


@BindingAdapter("useDefaults")
fun bindingDefaultsToRecycler(recycler: RecyclerView, useDefaults: Boolean) {
    if (useDefaults) {
        recycler.layoutManager = LinearLayoutManager(
                recycler.context,
                LinearLayoutManager.VERTICAL,
                false)
    }
}