package com.wild.touche.paytouch.celebrities.core.ui

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout


@BindingAdapter("visible")
fun bindingSwipeRefreshLayoutVisible(view: SwipeRefreshLayout, visible: Boolean) {
    view.isRefreshing = visible
}

@BindingAdapter("onRefresh")
fun bindingSwipeRefreshLayoutOnRefreshListener(view: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener) {
    view.setOnRefreshListener(listener)
}