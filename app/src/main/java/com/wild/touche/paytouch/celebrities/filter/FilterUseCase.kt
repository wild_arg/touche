package com.wild.touche.paytouch.celebrities.filter

import com.wild.touche.paytouch.celebrities.data.FilterParamsRepo
import com.wild.touche.paytouch.celebrities.data.LocationRepo
import com.wild.touche.paytouch.celebrities.di.AppComponent
import com.wild.touche.paytouch.celebrities.domain.FilterParams
import io.reactivex.Completable
import io.reactivex.Observable


interface FilterUseCase {
    fun observeParams(): Observable<FilterParams>
    fun observeLocations(): Observable<List<String>>
    fun setParams(params: FilterParams): Completable
}


class FilterUseCaseImpl(injector: AppComponent) : FilterUseCase {

    private val filterRepo: FilterParamsRepo by lazy { injector.filterParamsRepo() }
    private val locationsRepo: LocationRepo by lazy { injector.locationsRepo() }

    override fun observeParams(): Observable<FilterParams> =
            filterRepo.observe()

    override fun observeLocations(): Observable<List<String>> =
            locationsRepo.observe()

    override fun setParams(params: FilterParams): Completable =
            filterRepo.setFilter(params)

}